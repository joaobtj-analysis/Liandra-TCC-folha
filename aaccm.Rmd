

#AACCM - Área abaixo da curva de crescimento micelial
##Tabela dos valores brutos da aaccm calculada
```{r print data.frame aaccm, echo=FALSE}
library(knitr)
library(tidyr)
# kable(spread(data.aaccm, rep, aaccm))
```

##Boxplot com os tratamentos
```{r}
plot(aaccm~trat, data=data.aaccm)
```

##Anova e pressupostos
```{r anova aaccm}
anv.aaccm <- lm(aaccm ~ trat, data = data.aaccm)
anova(anv.aaccm)

#pressupostos anova
library(moments)
library(HH)
stem(anv.aaccm$residuals)
kurtosis(anv.aaccm$residuals)
skewness(anv.aaccm$residuals)
shapiro.test(anv.aaccm$residuals)
hov(aaccm ~ trat, data = data.aaccm)
```

```{r, fig.height=4}   
plot(anv.aaccm)
```

```{r, fig.height=4}
hovPlot(aaccm~trat, data=data.aaccm)
```

## Teste de Tukey
```{r tukey aaccm, fig.height=3}
library(emmeans)
tk.trat <- emmeans(anv.aaccm, ~trat, contr = "tukey")
CLD(tk.trat$emmeans, Letters=letters, reversed = TRUE)
tk.trat$contrasts
confint(tk.trat)$contrasts
plot(tk.trat$emmeans, horizontal=FALSE)
```

## Teste de Dunnett
```{r dunnett aaccm, fig.height=3, fig.pos="H"}
library(emmeans)
which(names(table(data.aaccm$trat))=="test")
dn.trat <- emmeans(anv.aaccm, ~trat, contr = "dunnett",ref=which(names(table(data.aaccm$trat))=="test"))
dn.trat
confint(dn.trat)$contrast
plot(dn.trat$emmean, horizontal=FALSE)
```

##PIC - porcentagem de inibição do crescimento micelial
```{r PIC, echo=FALSE}
max.pic <- as.numeric(with(data.aaccm,
                       aggregate(aaccm~trat,
                                 FUN=mean,
                                 subset(data.aaccm,trat=="test")))[2])
PC <- data.aaccm$aaccm/(max.pic)*1;
PIC <- (1-PC)*100
data.aaccm <- cbind(data.aaccm, PIC)
PIC.m <- with(data.aaccm,(aggregate(PIC~trat, FUN=mean))) #calculo das médias
PIC.m
```
